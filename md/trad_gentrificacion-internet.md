# La gentrificación del internet

Este es un ensayo sobre tecnología, relaciones de poder y dignidad
básica. Se trata acerca de la comercialización de las plataformas
_web_ y las dificultades de retener el poder individual y la
autonomía en línea. Versa sobre la gentrificación del internet.
Cuando digo que el internet ha sido gentrificado, estoy describiendo
los cambios en el poder y el control que limitan lo que podemos
hacer en línea. También estoy nombrando a una economía y una
industria que le da prioridad a las ganancias por parte de corporaciones
sobre el bien público; así como señalo las maneras en como ciertos
tipos de comportamiento en línea se han convertido en la manera
«correcta» de usar la _web_, mientras que otras formas de conducta
se les da la espalda o son tildadas de anacrónicas. En sus primeros
días la _web_ estaba siendo manejada por expertos en tecnología,
la curiosidad y el trabajo de las comunidades de «Hágalo Usted
Mismo» (+++DIY+++ por sus siglas en inglés) que se comunicaban
con extraños de todo el mundo. La _web_ que tenemos ahora está
guiada por diferentes principios, como los modelos comerciales
que dependen de una constante transferencia de datos[^1] de las
personas hacia los empresarios, las normas sociales de consumo[^2]
y la autopromoción,[^3] además de la _cajanegrización_[^4] (de
_blackboxing_,[^5] un neologismo de creciente popularidad) de
los algoritmos que dan estructura a las plataformas que usamos.
El internet de manera creciente está provocando que nos aislemos
más, que seamos menos democráticos y que estemos a la merced
de las corporaciones y sus accionistas. Es decir, el internet
está cada vez más gentrificado.

Aquí estoy hablando a grandes rasgos ---por supuesto que las
corporaciones siempre han moldeado el aspecto y el recibimiento
del internet y desde luego las comunidades +++DIY+++ aún son
una parte importante de la vida en línea---. Pero no podemos
negar el hecho de que un pequeño número de poderosas empresas
han empezado a ejercer un control significativo en torno a cómo
se ve y se percibe la _web_. Como Siva Vaidhyanathan[^6] lo ha
mencionado, una simple compañía, Facebook, domina el mercado
de los usuarios de las redes sociales, trasladando enormes cantidades
de poder económico y político a una sola corporación. Mientras
tanto, Google avasalla el 75% del mercado global de las búsquedas
en línea.[^7] El segundo buscador más popular, Bing, ni siquiera
se le acerca. La tienda de Amazon ha redefinido la manera en
como se compra en línea por medio de la predicción de nuestros
intereses y la transformación de nuestras expectativas sobre
la compra y la venta de bienes. El poder está tan concentrado
que no solo es inconveniente vivir sin las cinco más grandes
compañías tecnológicas: es imposible; esto ha provocado que ciertas
personas pidan dar fin a la confianza en estas empresas ---véase
aquí[^8], aquí[^9] y aquí[^10]---. Las corporaciones y sus recursos
prácticamente ilimitados han monopolizado la cultura digital,
dejando fuera de juego a las pequeñas empresas y plataformas
y, en el proceso, definiendo qué tipo de interacciones en línea
son posibles. Esta concentración del poder de control va más
allá de la reducción de las opciones del consumidor: es una forma
de gentrificación tecnológica.

Cuando las personas relacionan la gentrificación con el internet
por lo general es en torno a cómo la industria tecnológica ha
remodelado los vecindarios[^11] donde se alojan sus oficinas
centrales.[^12] Estos problemas ---muy reales[^13] y demasiado
importantes[^14]--- son más sobre cómo las empresas han generado
desigualdades en los lugares y las comunidades que están alrededor
de sus oficinas corporativas. En esto veo una serie de aspectos
relacionados pero distintos a los tipos de espacios y las maneras
de relacionarse en línea que son cada vez más fomentados o restringidos.
Cuando digo que el internet actual está gentrificado tengo un
doble objetivo: diagnosticar un conjunto de problemas y esbozar
lo que los ciberactivistas pueden hacer para demandar mayores
protecciones y espacios para ejercer la libertad.

Antes de llegar ahí es importante aclarar qué es la gentrificación
y cómo esta ayuda en la descripción de la _web_ moderna y convencional.
El termino «gentrificación» causa divisiones, donde algunos ven
oportunidades[^15] para el desarrollo económico,[^16] otros observan
una sentencia de muerte de las historias sociales y culturales
de las comunidades locales.[^17] Para hacer las cosas aún más
complicadas, no es que exista «una cosa» llamada «gentrificación»;
en su lugar se trata de un montón de procesos que se sobreponen
y que compiten entre sí en relación con las instituciones y las
partes interesadas. Un punto de partida es desde los estudios
de urbanística donde la gentrificación se define como

> un proceso económico y social por el cual el capital privado
> ---las inmobiliarias y las promotoras---, los propietarios de inmuebles
> y los arrendadores reinvierten en vecindarios descuidados a
> través de la rehabilitación de las viviendas, conversiones del
> desván y la construcción de nuevos inmuebles. A diferencia de la
> renovación urbana, la gentrificación es un proceso gradual que
> ocurre en un edificio o una cuadra a la vez; esto provoca una
> lenta reconfiguración del paisaje comercial y residencial del
> vecindario al desplazar los residentes pobres y de la clase
> trabajadora que ya no pueden costearse la vivienda en los
> vecindarios «revitalizados» debido al aumento de la renta y
> de los impuestos a la propiedad, y por los nuevos negocios
> que atienden a clientes con un mayor poder adquisitivo.
> (Perez, 2004, p. 139)[^18]

Las personas que ven la gentrificación como algo bueno tienden
a enfatizar las oportunidades de nuevos negocios y del desarrollo
de los bienes raíces. Pero estos beneficios no son distribuidos
equitativamente[^19] ---por lo general van a las personas que
ya tienen riqueza y recursos acumulados---. La gentrificación
modifica los espacios físicos del vecindario al atraer nuevas
estéticas arquitectónicas y otros tipos de negocios. Las casas
existentes lucen más pequeñas y anticuadas así como los viejos
negocios pierden clientes porque los nuevos inquilinos exigen
gustos más refinados. La gentrificación también transforma las
normas sociales del vecindario, lo que aumenta los posibles conflictos
debido al ruido,[^20] a los estilos de crianza[^21] e incluso
a las mascotas.[^22]

A través de distintas ciudades y vecindarios, la gentrificación
exacerba la inequidad y normaliza ciertos valores sociales mientras
excluye otros. Con estas tensiones en mente, ¿qué es lo que con
exactitud caracteriza al internet gentrificado? ¿Cómo podemos
traducir las condiciones de la gentrificación urbana a las plataformas
digitales? Observo tres aspectos clave de la gentrificación en
la _web_ de nuestros días; todas ellas limitan las libertades
en línea de los individuos en pos de los intereses de las compañías
tecnológicas más grandes.

_Insolación_. La gentrificación da como resultado una gran insolación
cuando los antiguos inquilinos son puestos en nuevos vecindarios
con diferentes niveles de ingresos y, por lo general, con distintas
expectativas sociales y culturales sobre el comportamiento del
vecindario. Los vecinos pueden terminar profundamente segregados[^23]
al acabar por vivir juntos pero asistir a diferentes iglesias,
enviar a sus hijos a otras escuelas o comprar en distintas tiendas.
Compárese esto con los filtros burbuja en línea. Antes de las
redes sociales, la formación de comunidades en línea casi siempre
implicaba el encuentro de nuevas personas con un interés en común.
No existían los algoritmos ni las plataformas basadas en las
recomendaciones de amistades o de contenido; solo aparecían en
el foro o en los _chat_ para ver quién más estaba ahí. (Por supuesto
el ingreso quedaba restringido a quién podía costearse un módem
y el tiempo para aprender a usarlo.) Las plataformas como Reddit
o 4chan aún operan de la misma manera, pero la mayoría de las
redes sociales usan las redes personales de la vida real para
vincular usuarios y publicar contenidos. Con el tiempo la generación
de contenido basado en los «me gusta» y las afinidades personales
se convirtieron en la norma, lo que resultó en lo que Eli Pariser[^24]
ha denominado «filtros burbuja». En lugar de estar expuesto a
una diversidad de gente y de contenido, las personas se están
segregando cada vez más.[^25] De manera particular lo problemático
de esta insolación en línea es que afuera de la _web_ la mayoría
de las personas ya emplean filtros burbuja para sus relaciones
sociales;[^26] esto significa que tendemos a tener amigos con
los mismos trasfondos racial y de clase. La promesa de que las
primeras comunidades en línea nos estaban sacando de nuestras
burbujas ha perdido demasiada relevancia en las redes sociales
más usadas.

_Aumento de los costos_. La gentrificación es en lo fundamental
una cuestión acerca del espacio, pero es un proceso que se despliega
con el tiempo. De manera gradual los nuevos vecinos incrementan
el valor de la propiedad y de los impuestos. El aumento de los
costos hacen que las relaciones comunitarias no sean posibles
a largo plazo para los residentes originales y las personas con
los mismos índices demográficos. En línea la gentrificación ocurre
cuando las viejas plataformas se esfuerzan por competir en contra
de los recursos y los valores de las nuevas plataformas. En mi
investigación sobre las contraculturas digitales[^27] he encontrado
que las comunidades marginadas luchan para que las tecnologías
más empleadas se adapten a sus necesidades. Para las comunidades
que habían estado en línea desde hace mucho tiempo, la competencia
contra las nuevas plataformas como Facebook o Instagram representó
una batalla perdida porque fueron sobrepasados y su código se
tornó obsoleto por los aparentes recursos interminables de las
grandes compañías tecnológicas.

_Comercio desigual_. La gentrificación no es acerca de quién
vive dónde: es en torno a qué tipos de negocios pueden ser sostenidos
por la comunidad que los rodea.[^28] La gentrificación por lo
regular implica la destrucción de los comercios locales que daban
soporte a las comunidades establecidas con el fin de hacer lugar
a otros negocios que atraen a los nuevos inquilinos. En mi vecindario,
en el sur de Filadelfia, he observado cómo los comercios locales,
como las tiendas de abarrotes, los restaurantes y los centros
comunitarios, se han transformado en estudios de yoga, _gastropubs_[^29]
y _almorzadores_. Una de las crueldades perversas de la gentrificación
es cómo la percepción de la alteridad pasa de los viejos a los
nuevos residentes. Los vecindarios con riquezas histórica y de
cultura comunitaria son entendidos en contraposición de las expectativas
y los valores de los nuevos inquilinos. Debido a su mayor cantidad
de recursos y de influencia, por lo general los nuevos residentes
hacen prevalecer sus valores e intereses. También acontecen cambios
cruciales en las normas sociales del internet gentrificado. Por
ejemplo, he estado escribiendo una historia social de Craiglist[^30]
y una y otra vez oigo personas que describen al sitio como pasado
de moda. En entrevistas he escuchado frases como «el internet
para los pobres» o «el sitio para la clase trabajadora» para
describir la plataforma que se percibía como elitista y visionaria
cuando en 1996 se puso en línea. Pero mientras nuevos sitios
emergen con más características y nuevas estéticas, Craiglist
ha empezado a verse como de más baja calidad y más turbio; sin
importar que siga prestando los mismos servicios, la mayoría
de las personas lo ven como un sitio retrógrada, anticuado y
al punto de la obsolescencia. El riesgo aquí es cómo de manera
activa se dejan atrás personas y plataformas no porque han dejado
de ser útiles o de funcionar, sino debido a que ya no se ven
ni se sienten como el resto de la _web_.

## ¿Qué podemos hacer?

El trabajo de combatir la gentrificación no implica poner fuera
de juego a Facebook o catalogar a la _web_ convencional como
irrelevante. Un objetivo más inmediato consiste en solo diagnosticar
el conjunto de problemas y los pasos sugeridos para demandar
un cambio. Aquí hay algunas ideas que nos pueden ayudar a comenzar.

_Sé tu propio algoritmo_. En lugar de aceptar pasivamente las
redes y los contenidos con los que nos alimentan las plataformas,
necesitamos apropiarnos más del aspecto de nuestras redes para
así poder diversificar el contenido que cae en nuestras manos.
Plataformas como Facebook[^31] y YouTube[^32] están retocando
sus algoritmos para videos recomendados porque han sido presionados
por haber facilitado la difusión de noticias virales y de contenidos
violentos. Pero mientras las plataformas trabajan en solucionar
este problema, podemos dar los primeros pasos para ser nuestros
propios algoritmos y de manera deliberada diversificar nuestras
redes y los contenidos que vemos. En un sentido práctico esto
significa que llevemos acabo inspecciones casuales de las personas
que seguimos, además de preguntarte cómo puedes diversificar
las voces y los puntos de vista. Esto puede llevarnos a buscar
más personas de color, mujeres, sujetos _queer_, personas con
capacidades diferentes o gente neurotípica; incluso podríamos
hacer el esfuerzo de exponernos a contenidos de personas que
viven en zonas rurales o en otros países. La sacudida de nuestras
redes puede generar más conciencia acerca de cómo las plataformas
operan y tal vez recuperar un poco el entusiasmo de los primeros
años de la _web_ en torno al aprendizaje de nuevas perspectivas
debido al encuentro con otras personas.

_En la ciudad, como en línea, necesitamos regulaciones_. Así
como las ciudades batallan por dar con intervenciones[^33] que
funcionen[^34] desde los gobiernos locales, es cada vez más claro
que los políticos de +++EE. UU.+++ están desinformados[^35] e
indispuestos a intervenir cuando se trata de confrontar a las
grandes empresas tecnológicas.[^36] Pero exigir a los legisladores
de todos los niveles que hagan su trabajo puede marcar una enorme
diferencia. Así como ir a las juntas locales de zonificación
puede ayudar a que los nuevos residentes entiendan los conflictos
del vecindario, el aprendizaje de las políticas básicas de las
plataformas _web_ no es difícil, solo toma un poco de tiempo.
¿Cuántos proveedores de internet hay en nuestro vecindario? ¿Cuántos
representantes locales aceptan donaciones de los grandes proveedores
de internet como Comcast? Empieza con los diputados y los representantes
de tu ayuntamiento, ambos tendrán asistentes que responderán
el teléfono en lugar de mandarte al buzón de voz. Pregúntales
sobre su posición respecto a la neutralidad en la red, la penetración
del internet y el apoyo local para la alfabetización digital.
Estar informado es una parte crucial para entender las barreras
que detienen el cambio radical.

## Fuente

Traducción de «The gentrification of the internet» de Jessa Lingel,
publicado en _Culture Digitally_ el 13 de marzo del 2019. Disponible
en [alturl.com/56n82](http://alturl.com/56n82).

[^1]: [alturl.com/8gasy](https://www.nytimes.com/2018/01/30/opinion/strava-privacy.html)
[^2]: [alturl.com/6au9d](https://www.healthline.com/health-news/social-media-use-increases-depression-and-loneliness)
[^3]: [alturl.com/jbuem](https://yalebooks.yale.edu/book/9780300209389/status-update)
[^4]: [alturl.com/j3w24](http://www.hup.harvard.edu/catalog.php?isbn=9780674368279)
[^5]: [alturl.com/bszcs](https://es.wikipedia.org/wiki/Cajanegrizar)
[^6]: [alturl.com/zih3d](http://www.worldcat.org/oclc/1050083621)
[^7]: [alturl.com/awd54](https://www.smartinsights.com/search-engine-marketing/search-engine-statistics/)
[^8]: [alturl.com/k9puy](https://www.nytimes.com/2017/04/22/opinion/sunday/is-it-time-to-break-up-google.html?_r=0)
[^9]: [alturl.com/v5bvd](https://www.theringer.com/tech/2018/11/19/18102162/tim-wu-facebook-antitrust-law-book-curse-of-bigness)
[^10]: [alturl.com/zqiaf](https://www.cnbc.com/2019/03/08/elizabeth-warren-pushes-to-break-up-companies-like-amazon-and-facebook.html)
[^11]: [alturl.com/7j3sd](https://www.newsweek.com/san-francisco-tech-industry-gentrification-documentary-378628)
[^12]: [alturl.com/7uus5](https://www.wired.com/2017/02/tech-campuses-hinder-diversity-help-gentrification/)
[^13]: [alturl.com/kk3v4](https://www.huffingtonpost.com/entry/silicon-valley-gentrification-low-wage-workers_us_56d08998e4b0871f60eb3318)
[^14]: [bit.ly/2MwI0Lj](https://www.citylab.com/equity/2015/09/the-complicated-link-between-gentrification-and-displacement/404161/)
[^15]: [alturl.com/b3cr4](https://www.fastcompany.com/3026000/why-gentrification-can-be-a-good-thing)
[^16]: [alturl.com/k6syf](https://www.theatlantic.com/business/archive/2015/06/gentrification-bad-word/396908/)
[^17]: [alturl.com/si8p9](https://nymag.com/news/intelligencer/62675/)
[^18]: [alturl.com/iqnfq](http://www.worldcat.org/oclc/865276680)
[^19]: [alturl.com/gk6q9](https://www.theatlantic.com/magazine/archive/2014/06/the-case-for-reparations/361631/)
[^20]: [alturl.com/nyaqg](https://psmag.com/social-justice/gentrification-increaes-noise-complaints-in-nyc)
[^21]: [alturl.com/t5bgj](https://shelterforce.org/2016/02/04/gentrification_and_public_schools_its_complicated/)
[^22]: [bit.ly/2NsDzph](https://www.citylab.com/environment/2017/08/the-politics-of-the-dog-park/536463/)
[^23]: [alturl.com/k359s](https://www.coloradotrust.org/content/story/thread-ties-segregation-gentrification)
[^24]: [alturl.com/uqp7p](http://www.worldcat.org/oclc/847365101)
[^25]: [alturl.com/etbe8](https://www.mediamatters.org/blog/2018/04/13/lack-diversity-core-social-medias-harassment-problem/219932)
[^26]: [alturl.com/thg6w](http://www.leonidzhukov.net/hse/2017/networkscience/papers/McPherson_HomophilyInSocialNetworks.pdf)
[^27]: [alturl.com/vpkkt](https://mitpress.mit.edu/books/digital-countercultures-and-struggle-community)
[^28]: [alturl.com/aw3ry](http://www.governing.com/columns/assessments/gov-gentrification-local-business-extinction.html)
[^29]: [alturl.com/mvcf6](https://es.wikipedia.org/wiki/Gastropub)
[^30]: [alturl.com/qsmbf](https://www.tandfonline.com/doi/abs/10.1080/24701475.2018.1478267)
[^31]: [alturl.com/ryitt](https://mashable.com/article/facebook-fake-news-uk/#uuwQiAvXvkqk)
[^32]: [wapo.st/2KRz0CE](https://www.washingtonpost.com/technology/2019/01/25/youtube-is-changing-its-algorithms-stop-recommending-conspiracies/?utm_term=.5b6009428c39)
[^33]: [alturl.com/59hgi](https://newrepublic.com/article/144260/stop-gentrification)
[^34]: [alturl.com/4hcrq](https://abc7ny.com/realestate/newark-out-to-protect-low-income-residents-from-gentrification/4848609/)
[^35]: [alturl.com/grzyg](https://www.cnet.com/news/some-senators-in-congress-capitol-hill-just-dont-get-facebook-and-mark-zuckerberg/)
[^36]: [wapo.st/2JmPvDv](https://www.washingtonpost.com/business/technology/lawmakers-agree-social-media-needs-regulation-but-say-prompt-federal-action-is-unlikely/2018/04/11/d3ce71b0-3daf-11e8-8d53-eba0ed2371cc_story.html?noredirect=on&utm_term=.7fe9d958d410)
